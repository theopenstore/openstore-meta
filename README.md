# The OpenStore

The open source app store for Ubuntu Touch and snaps.

This is the bug tracker for the OpenStore website, api, and app.

## App

* [Code](https://gitlab.com/theopenstore/openstore-app)
* [Bugs](https://gitlab.com/theopenstore/openstore-meta/issues)
* [Translations](https://translate.ubports.com/projects/openstore/openstore-app/)
* [Get the app](https://open-store.io/docs#install)

## Web

* [Code](https://gitlab.com/theopenstore/openstore-web)
* [Bugs](https://gitlab.com/theopenstore/openstore-meta/issues)
* [Translations](https://translate.ubports.com/projects/openstore/open-storeio/)
* [Submit your apps](https://open-store.io/submit)

## Api

* [Code](https://gitlab.com/theopenstore/openstore-api)
* [Bugs](https://gitlab.com/theopenstore/openstore-meta/issues)
* [Translations](https://translate.ubports.com/projects/openstore/openstore-api/)
